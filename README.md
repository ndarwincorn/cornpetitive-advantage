# Cornpetitive Advantage

#### *Note: This repo is for the Hugo theme based on my blog. If you're looking for the actual blog's code, you can find that [here](https://gitlab.com/ndarwincorn/ndc.me).*

Cornpetitive Advantage is a two-column Hugo theme for personal sites. See the live demo (from the eponymous blog) [here](https://ndarwincorn.me).

![Cornpetitive Advantage Theme screenshot](https://gitlab.com/ndarwincorn/cornpetitive-advantage/uploads/86495ca6b3b2ba29110d6aacf34b277d/example-screenshot.png)

This theme is designed to be simple and unobtrusive, featuring a clean color palette and consistent formatting throughout, with an emphasis on a beautiful hero image as the masthead. [Isso](https://posativ.org/isso/) and default taxonomies are supported out of the box.

## Getting started

I'm assuming you've already installed Hugo and have created a new project. You can follow instructions for that [here](https://gohugo.io/getting-started/installing/).

1. In the root folder of your Hugo project, run:

    ```bash
    git submodule add https://gitlab.com/ndarwincorn/cornpetitive-advantage.git themes/cornpetitive-advantage
    ```

2. The `exampleSite` directory contains all of the necessary resources to launch the site. Due (kinda) to [minimal-academic#3](https://github.com/jhu247/minimal-academic/issues/3), it will fail until you [symlink the themes folder from your main project](#symlinking). Run `hugo server` from here and point your browser to `http://localhost:1313/` to test it out.

3. Copy `config.toml` from `exampleSite` to your project's root folder to use it. This is also where you'll change the title of your site, add your own social media links, configure Contact Page email settings, Isso commenting, etc.

4. Upload your own avatar and hero image by replacing `portrait.jpg` and `hero.jpg` in `static/img/`.

5. Create some content! Either copy the `.md` files from `exampleSite` as a template or use the [hugo new](https://gohugo.io/commands/hugo_new/) command.

## Helpful tips

* Running `hugo server` with the `--watch` and `--verbose` flags makes development much faster and debugging much easier.
* Sometimes your browser will cache resources to improve performance, so your changes will not appear. A cache-clearing refresh (^<F5> on most browsers) will fix this.

## Contributing

There's still more to do:

* i18n support
* support for [Fathom](https://usefathom.com/), or other FOSS analytics platforms
* support for non-Isso FOSS commenting software
* night-mode toggle

Certain support will never be added:

* non-FOSS integration (Disqus, Google Analytics, etc.; for more info read the [blog](https://ndarwincorn.me))

Please feel free to submit an issue or create a pull request!

## License

This theme is released under the MIT License. For more information, please read the [license](https://gitlab.com/ndarwincorn/cornpetitive-advantage/blob/master/LICENSE).

## Credits

* Jonathan Hu's [Minimal Academic](https://github.com/jhu247/minimal-academic), and the inspirations that kicked him off on the theme build.

## Symlinking

To trick Hugo's built-in server to thinking that the example site is a real site, symlink the themes folder from your root project into the `exampleSite` directory:

### bash
```bash
$ pwd
/path/to/my/hugo/project
$ ln -s /path/to/my/hugo/project/themes /path/to/my/hugo/project/themes/cornpetitive-advantage/exampleSite/themes
```

### pwsh
###### *Note: this is from memory, so better to consult the PowerShell docs before blindly running this, since Windows requires admin privileges to create symlinks*

```pwsh
# $env:PWD
PATH:\to\my\hugo\project
# New-Item -ItemType "SymbolicLink" -Value "PATH:\to\my\hugo\project\themes" -Name "PATH:\to\my\hugo\project\themes\cornpetitive-advantage\exampleSite\themes"
```
